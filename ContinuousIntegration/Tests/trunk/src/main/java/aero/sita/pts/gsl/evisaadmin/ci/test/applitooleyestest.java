package aero.sita.pts.gsl.evisaadmin.ci.test;


import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.epam.reportportal.testng.ReportPortalTestNGListener;






/**
 * Runs Applitools test for the demo app https://demo.applitools.com
 */
@Listeners({ReportPortalTestNGListener.class})
public class applitooleyestest {
    protected static final Logger LOGGER = Logger.getLogger(applitooleyestest.class);


    @Test
    public void publicportallogintest() {
        System.setProperty("webdriver.chrome.driver",
                "G:\\POC & Knowledge Base\\Selenium\\Selenium-ReportPortal-Project\\Dependency_JAR_for_bsaic_project\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        String baseUrl = "https://evisa.rop.gov.om/en/evisalogin";
        String expectedTitle = "evisalogin - Evisa";
        String actualTitle = "";
        driver.get(baseUrl);
        LOGGER.info("System will load evisa PP url");
        actualTitle = driver.getTitle();
        LOGGER.warn("Page load will take some time");
        if (actualTitle.contentEquals(expectedTitle)) {
            System.out.println("Login test passed");
        } else {
            System.out.println("Test Failed");
        }
        driver.close();
    }
}
